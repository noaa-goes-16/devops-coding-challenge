# DevOps Coding Challenge

We have provided a python script `download_goes16.py` which is written to interface with the NOAA GOES-16 S3 bucket on the [AWS Registry of Open Data](https://registry.opendata.aws/noaa-goes/). An AWS account is not needed to perform actions on this AWS bucket. 

About GOES:
> GOES satellites (GOES-16, GOES-17, & GOES-18) provide continuous weather imagery and monitoring of meteorological and space environment data across North America. GOES satellites provide the kind of continuous monitoring necessary for intensive data analysis.

## Prompt

1. Write a Dockerfile which installs the dependencies necessary for executing the Python script.  Use `ubuntu:22.04` as your base image.
2. Provide documentation for how to build the Docker image and how to run the Docker container to execute the script
3. Push the code to GitLab and write a `.gitlab-ci` file which 
    - Tests the script using PyTest
    - Builds the Docker image
    - Pushes the Docker image to the GitLab Container Registry


Share your repository with `jay@chloris.earth` and `jarrod@chloris.earth`. This task should take roughly 1 hour of your time.

# How to:
## Run the script:
```
python src/download_goes16.py ABI-L2-ACHAC/2022/327/17/OR_ABI-L2-ACHAC-M6_G16_s20223271736175_e20223271738548_c20223271741416.nc .
```

## Run PyTest:
```
pytest tests/
```