import os

import pytest

OUTPUT_DIR = os.path.join(os.path.dirname(__file__), "output")


@pytest.fixture
def output_dir():
    os.makedirs(OUTPUT_DIR, exist_ok=True)
    return OUTPUT_DIR
