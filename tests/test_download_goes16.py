import os


import download_goes16 as download_goes16


def test_download_file(output_dir):
    file_path = "ABI-L2-ACHAC/2022/327/17/OR_ABI-L2-ACHAC-M6_G16_s20223271736175_e20223271738548_c20223271741416.nc"
    download_goes16.download_file(file_path, output_dir)

    assert os.path.exists(os.path.join(output_dir, os.path.basename(file_path)))
