FROM ubuntu:22.04

# Install Python and other packages

RUN apt-get update && apt-get install -y python3 python3-dev python3-pip build-essential 

# Copy the requirements file and install the packages
COPY requirements.txt /app/requirements.txt
RUN pip3 install -r /app/requirements.txt

COPY . /app

# Working directory
WORKDIR /app

# Running the Python script
ENTRYPOINT ["python3", "src/download_goes16.py"]
CMD ["ABI-L2-ACHAC/2022/327/17/OR_ABI-L2-ACHAC-M6_G16_s20223271736175_e20223271738548_c20223271741416.nc", "."]