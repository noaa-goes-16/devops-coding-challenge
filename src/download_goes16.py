import argparse
import os

import boto3
from botocore import UNSIGNED
from botocore.client import Config


def download_file(file_path: str, output_dir: str) -> None:
    """Download a file from the NOAA GOES-16 S3 bucket and save it to a local directory"""
    client = boto3.client(
        "s3", region_name="us-east-1", config=Config(signature_version=UNSIGNED)
    )

    client.download_file(
        "noaa-goes16", file_path, os.path.join(output_dir, os.path.basename(file_path))
    )


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("file_path", type=str)
    parser.add_argument("output_dir", type=str)
    args = parser.parse_args()

    download_file(args.file_path, args.output_dir)


if __name__ == "__main__":
    main()
